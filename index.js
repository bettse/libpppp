const log = {
  debug: require('debug')('client:debug'),
};

const {CloudClient} = require('./libpppp');

const servers = ['3.10.99.101', '139.9.86.167', '3.227.45.161'];

async function main(did) {
  const opts = {
    servers,
    encryptionKey: 'camera',
  };

  const cloud = new CloudClient(opts);

  const {sockAddr} = await cloud.hello();
  log.debug(sockAddr);

  const camera = await cloud.getCamera(did);
  if (!camera) {
    console.log('no camera');
    return false;
  }

  camera.sockAddrs.push({
    family: 2,
    port: camera.sockAddrs[0].port,
    address: '10.0.1.74',
  });

  const punch = await camera.punch();
  log.debug({punch});

  await camera.checkUser();

  //console.log(await camera.getRecordDays());
  const list = await camera.getRecordList();
  console.log({list});
  /*
  console.log(await camera.getDatetime());
  console.log(await camera.getWifi());
  console.log(await camera.getAlarms());
  console.log(await camera.getParams());
  console.log(await camera.getVideoFiles());
  console.log(await camera.prStart());
  console.log(await camera.checkOta());
  */
  return camera.close();
}

if (process.argv.length > 2) {
  const did = process.argv[2];
  main(did).then(console.log).catch(console.log);
} else {
  console.log('node index.js <device_id>');
}

const dgram = require('dgram');
const fs = require('fs');
const EventEmitter = require('events');

const DECIMAL = 10;

// Break this into cloud/camera
const log = {
  send: require('debug')('libpppp:send'),
  recv: require('debug')('libpppp:receive'),
  msg: require('debug')('libpppp:msg'),
  //DRW
  ack: require('debug')('libpppp:ack'),
  multipart: require('debug')('libpppp:multipart'),
  playback: require('debug')('libpppp:playback'),
  text: require('debug')('libpppp:text'),
  json: require('debug')('libpppp:json'),
  audio: require('debug')('libpppp:audio'),
  video: require('debug')('libpppp:video'),
};
const {encrypt, decrypt} = require('./crypt');
const {
  decodeSockAddr,
  encodeSockAddr,
  decodeDid,
  encodeDid,
} = require('./utils');
const {types} = require('./types');

const DRW_MAGIC = 0xd1;
const PPPP_MAGIC = 0xf1;
const defaultPort = 32100;
const packetInterval = 1000;
const keepaliveInterval = 100;

const channels = {
  text: 0,
  video: 1,
  audio: 2,
  playback: 4,
  unknown: 5,
};

// NOTE: DRW: device read/write

const DRW_COMMANDS = {
  GET_PARAMS: 0x65,
  DEV_CONTROL: 0x66,
  GET_ALARMS: 0x6b,
  CHECK_USER: 0x6e,
  STREAM: 0x6f,
  GET_WIFI: 0x70,
  SCAN_WIFI: 0x71,
  GET_VIDEOFILES: 0x74,
  GET_DATETIME: 0x7d,
  UPLOAD_FILE_REQUEST: 0xc9,
  UPLOAD_FILE: 0xca,
  GET_RECORD_DAY: 0xcd,
  GET_RECORD_LIST: 0xce,
  PLAY_RECORD_FILE: 0xcf,
  PR_START: 0x139,
  GET_RECORD_HOUR: 0x191,
  GET_RECORD_MIN: 0x192,
  CHECK_OTA: 0x196,
};

const DEVICE_STATUS = {
  '0': 'online',
  '-1': 'invalid did, valid prefix',
  '-2': 'offline',
  '-3': 'invalid prefix',
};

const printError = err => {
  if (err) {
    console.log(err);
  }
};

class Client {
  constructor(opts = {}) {
    const {socket, encryptionKey, sockAddrs} = opts;
    this.encryptionKey = encryptionKey;
    this.socket = socket;
    this.sockAddrs = sockAddrs;

    console.log('on', this.constructor.name);
    this.socket.on('message', (msg, rinfo) => {
      const {address, port} = rinfo;
      const valid_addresses = this.sockAddrs.map(sockAddr => sockAddr.address);
      if (!valid_addresses.includes(address)) {
        //console.log('ignoring packet for someone else');
        return;
      }
      msg = this.decrypt(msg);
      log.recv(this.constructor.name, msg.toString('hex'));
      this.onMessage(msg, rinfo);
    });
  }

  decrypt(msg) {
    return decrypt(this.encryptionKey, msg);
  }

  encrypt(msg) {
    return encrypt(this.encryptionKey, msg);
  }

  parseMessage(msg) {
    if (msg[0] !== PPPP_MAGIC) {
      console.log('Invalid PPPP packet', msg.toString('hex'));
      return;
    }
    const type = msg[1];
    const length = msg.readUInt16BE(2);
    const payload = msg.slice(4, 4 + length);

    switch (type) {
      case types.HELLO_ACK:
      case types.PUNCH_TO:
        return {type, sockAddr: decodeSockAddr(payload)};
      case types.P2P_REQ_ACK:
        const status = payload[0];
        return {type, status, online: status === 0x00};
      case types.PUNCH_PKT:
      case types.P2P_RDY:
        return {type, deviceId: decodeDid(payload)};
      case types.ALIVE:
      case types.ALIVE_ACK:
        return {type};
      case types.CLOSE:
        return {type};
      default:
        return {type, length, payload};
    }
  }

  sendPacket(type, payload = Buffer.from('')) {
    return new Promise((resolve, reject) => {
      const header = Buffer.from([PPPP_MAGIC, type, 0, 0]);
      header.writeUInt16BE(payload.length, 2);
      let pkt = Buffer.concat([header, payload]);

      log.send(pkt.toString('hex'));
      pkt = this.encrypt(pkt);

      this.sockAddrs.forEach(({address, port}) => {
        this.socket.send(pkt, port, address, (err, bytes) => {
          if (err) {
            return reject(err);
          }
        });
      });
      resolve();
    });
  }

  sendMessage(type, payload = Buffer.from('')) {
    log.msg(type.toString(0x10), payload.toString('hex'));
    return new Promise((resolve, reject) => {
      const repeat = setInterval(() => {
        this.sendPacket(type, payload);
      }, packetInterval);
      const ackType = type + 1;

      const self = this;
      console.log('on', this.constructor.name, type);
      this.socket.on('message', function waitForAck(msg) {
        const parsed = self.parseMessage(self.decrypt(msg));
        if (parsed.type === ackType) {
          clearInterval(repeat);

          console.log('off', self.constructor.name, type);
          self.socket.off('message', waitForAck);
          resolve(parsed);
        }
      });

      this.sendPacket(type, payload);
      //reject(); // timeout/ max retries?
    });
  }
}

class CloudClient extends Client {
  constructor(opts = {}) {
    const {servers, port = defaultPort} = opts;
    const sockAddrs = servers.map(server => {
      return {
        address: server,
        port: defaultPort,
        family: 2, // AF_INET
      };
    });
    const socket = dgram.createSocket('udp4');
    socket.bind();

    super({socket, sockAddrs, ...opts});

    this.cameraSockAddrs = [];
  }

  onMessage(msg) {
    const parsed = this.parseMessage(msg);
    const {type, sockAddr} = parsed;
    switch (type) {
      case types.PUNCH_TO:
        if (!this.cameraSockAddrs.includes(sockAddr) && sockAddr.family === 2) {
          this.cameraSockAddrs.push(sockAddr);
        }
        break;
    }
  }

  async hello() {
    const ack = await this.sendMessage(types.HELLO);
    const {sockAddr} = ack;
    const {address} = sockAddr;
    this.external_ip = address;
    return ack;
  }

  async getCamera(deviceId) {
    const payload = Buffer.alloc(36);
    const did = encodeDid(deviceId);
    did.copy(payload);
    const address = this.socket.address();
    const sockAddr = encodeSockAddr(address);
    sockAddr.copy(payload, 21);
    const req = await this.sendMessage(types.P2P_REQ, payload);
    if (!req.online) {
      return null;
    }

    return new Promise((resolve, reject) => {
      const wait = setInterval(() => {
        if (this.cameraSockAddrs.length === 0) {
          return;
        }
        const sockAddrs = [];
        while (this.cameraSockAddrs.length) {
          sockAddrs.push(this.cameraSockAddrs.pop());
        }
        clearInterval(wait);

        const opts = {
          deviceId,
          socket: this.socket,
          encryptionKey: this.encryptionKey,
          sockAddrs,
        };
        resolve(new DRWClient(opts));
      }, 100);
    });
  }
}

class CameraClient extends Client {
  constructor(opts = {}) {
    super(opts);
    this.deviceId = opts.deviceId;
  }

  startKeepalive() {
    if (this.keepalive) {
      return;
    }
    this.keepalive = setInterval(() => {
      this.sendPacket(types.ALIVE);
    }, keepaliveInterval);
    this.sendPacket(types.ALIVE);
  }

  stopKeepalive() {
    if (this.keepalive) {
      clearInterval(this.keepalive);
    }
  }

  onMessage(msg, rinfo) {
    const parsed = this.parseMessage(msg);
    const {type} = parsed;
    switch (type) {
      case types.PUNCH_PKT:
        this.punch(true);
        break;
      case types.P2P_RDY:
        const {address} = rinfo;
        // Remove others since we know what one responded
        this.sockAddrs = this.sockAddrs.filter(
          sockAddr => sockAddr.address === address,
        );
        this.startKeepalive();
        break;
      case types.ALIVE:
        this.sendPacket(types.ALIVE_ACK);
        break;
      case types.CLOSE:
        console.log('close');
        this.stopKeepalive();
        break;
    }
    return parsed;
  }

  // When we are responding to a PUNCH_PKT with one of our own, we only want to send one
  async punch(oneshot = false) {
    const payload = Buffer.alloc(20);
    encodeDid(this.deviceId).copy(payload);
    if (oneshot) {
      return this.sendPacket(types.PUNCH_PKT, payload);
    } else {
      return this.sendMessage(types.PUNCH_PKT, payload);
    }
  }

  close() {
    this.stopKeepalive();
  }
}

class DRWClient extends CameraClient {
  constructor(opts = {}) {
    super(opts);
    this.emitter = new EventEmitter();
    this.nextSequence = 0;

    this.lastPlaybackFrame = -1;
    this.lastFrame = -1;

    this.partial = {
      buffer: Buffer.alloc(0),
      total: 0,
      lastSequence: -1,
      mimetype: 0,
    };

    this.localVideoSocket = dgram.createSocket('udp4');

    // Will fail ofr serial < 1025
    const [prefix, serial, check] = this.deviceId.split('-');
    this.localVideoPort = parseInt(serial, DECIMAL);
  }

  onMessage(msg, rinfo) {
    const {type, payload} = super.onMessage(msg, rinfo);
    switch (type) {
      case types.DRW:
        const drw = this.decodeDrw(payload);
        this.drwAck(drw);
        // TODO: debounce emit
        if (drw.channel === channels.text && drw.body) {
          // TODO add channel type to emit eventName
          this.emitter.emit('drw', drw);
        }
        break;
      case types.DRW_ACK:
        log.ack(this.decodeDrwAck(payload));
        break;
    }
  }

  async drwAck({channel, sequence}) {
    // Trying to handle them one at a time for now.
    // Fake that I got two
    const count = 2;
    const payload = Buffer.alloc(4 + 2 * count);
    //console.log('drwAck', channel, sequence);
    payload[0] = DRW_MAGIC;
    payload[1] = channel;
    payload.writeUInt16BE(count, 2);
    for (var i = 0; i < count; i++) {
      payload.writeUInt16BE(sequence, 2 * i + 4);
    }
    this.sendPacket(types.DRW_ACK, payload);
    return this.sendPacket(types.DRW_ACK, payload);
  }

  async checkUser(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'check_user',
      cmd: DRW_COMMANDS.CHECK_USER,
      devmac: '0000',
      user,
      pwd,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async scanWifi(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'scan_wifi',
      cmd: DRW_COMMANDS.SCAN_WIFI,
      user,
      pwd,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async getWifi(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'get_wifi',
      cmd: DRW_COMMANDS.GET_WIFI,
      user,
      pwd,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async checkOta(user = 'admin', pwd = '6666', bin_path = '', md5_path = '') {
    const req = {
      pro: 'check_ota',
      cmd: DRW_COMMANDS.CHECK_OTA,
      user,
      pwd,
      bin_path,
      md5_path,
      otaType: bin_path === '' ? 2 : 1, //1 OTAServer, 2 OTAP2P
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async prStart(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'pr_start',
      cmd: DRW_COMMANDS.PR_START,
      user,
      pwd,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async getDatetime(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'get_datetime',
      cmd: DRW_COMMANDS.GET_DATETIME,
      user,
      pwd,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async getParams(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'get_params',
      cmd: DRW_COMMANDS.GET_PARAMS,
      user,
      pwd,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async getAlarms(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'get_alarms',
      cmd: DRW_COMMANDS.GET_ALARMS,
      user,
      pwd,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async getVideoFiles(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'get_videofiles',
      cmd: DRW_COMMANDS.GET_VIDEOFILES,
      user,
      pwd,
      start_time: '',
      end_time: '',
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async getRecordList(user = 'admin', pwd = '6666', ymd = '2021_03_27') {
    const req = {
      pro: 'get_record_list',
      cmd: DRW_COMMANDS.GET_RECORD_LIST,
      user,
      pwd,
      ymd,
    };
    const response = await this.sendJSON(JSON.stringify(req));

    const {cmd, result, record_num} = response;
    if (cmd !== req.cmd) {
      console.log('resolved with wrong command');
      return [];
    }
    if (result !== 0) {
      // I think this means an error
      console.log('resolved with non-zero result');
      return [];
    }
    const records = [];
    for (var i = 0; i < record_num; i++) {
      records.push({
        name: response[`record_name[${i}]`],
        size: response[`record_size[${i}]`],
      });
    }
    return records;
  }

  async getRecordDays(user = 'admin', pwd = '6666', year = 2021) {
    const req = {
      pro: 'get_record_day',
      cmd: DRW_COMMANDS.GET_RECORD_DAY,
      user,
      pwd,
      year,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async getRecordHourList(user = 'admin', pwd = '6666', ymd) {
    const req = {
      pro: 'get_record_hour',
      cmd: DRW_COMMANDS.GET_RECORD_HOUR,
      user,
      pwd,
      ymd,
    };
    const response = await this.sendJSON(JSON.stringify(req));

    return response;
  }

  async getRecordMinList(user = 'admin', pwd = '6666', ymdh) {
    const req = {
      pro: 'get_record_min',
      cmd: DRW_COMMANDS.GET_RECORD_MIN,
      user,
      pwd,
      ymdh,
    };
    const response = await this.sendJSON(JSON.stringify(req));

    return response;
  }

  async playRecordFile(user = 'admin', pwd = '6666', filename) {
    this.playbackFilename = filename;
    fs.writeFile(filename, Buffer.alloc(0), printError);
    const req = {
      pro: 'play_record_file',
      cmd: DRW_COMMANDS.PLAY_RECORD_FILE,
      user,
      pwd,
      //suspend: 0,
      type: 1,
      audio: 1,
      video: 1,
      filename,
    };
    return this.sendJSON(JSON.stringify(req));
  }

  async stream(user = 'admin', pwd = '6666') {
    const req = {
      pro: 'stream',
      cmd: DRW_COMMANDS.STREAM,
      user,
      pwd,
      video: 2, //1: 1280x720 | 2: 640x480
    };
    return this.sendJSON(JSON.stringify(req));
  }

  // TODO: sendHTTP, sendVideo, sendAudio, etc
  async sendJSON(json) {
    const jsonBuf = Buffer.from(json);
    const payload = Buffer.alloc(12 + jsonBuf.length);

    const sequenceNumber = this.nextSequence++;
    payload[0] = DRW_MAGIC;
    payload[1] = channels.text;
    payload.writeUInt16BE(sequenceNumber, 2);
    payload[4] = 0x06; // I think this means JSON
    payload[5] = 0x0a;
    payload[6] = 0xa0;
    payload[7] = 0x80;
    payload.writeUInt16LE(json.length, 8);
    jsonBuf.copy(payload, 12);

    await this.sendMessage(types.DRW, payload);

    return new Promise((resolve, reject) => {
      const self = this;
      let resolved = false;
      console.log('once', this.constructor.name, 'drw');
      this.emitter.once('drw', function waitForResponse(drw) {
        if (resolved) {
          // duplicate packets
          return;
        }
        // Can't check sequence number since the response could be multipart.
        // Don't need to check for channel because 'drw' is only emitted for text right now
        resolved = true;
        console.log('off', self.constructor.name, 'drw');
        self.socket.off('drw', waitForResponse);
        resolve(drw.body);
      });
    });
  }

  //
  // Handling incoming
  //

  decodeDrwAck(payload) {
    const channel = payload[1];
    const count = payload.readUInt16BE(2);
    const sequences = [];
    for (var i = 0; i < count; i++) {
      sequences.push(payload.readUInt16BE(i * 2 + 4));
    }
    return {channel, count, sequences};
  }

  decodeDrw(payload) {
    if (payload[0] !== DRW_MAGIC) {
      console.log('Invalid DRW packet');
    }
    const channel = payload[1];
    const sequence = payload.readUInt16BE(2);
    switch (channel) {
      case channels.text:
      case channels.unknown:
        return this.decodeDrwText({channel, sequence, payload});
      case channels.video:
        log.video(channel, sequence);
        return this.decodeDrwVideo({channel, sequence, payload});
      case channels.audio:
        log.audio(channel, sequence);
        return {channel, sequence};
      case channels.playback:
        return {
          channel,
          ...this.decodeDrwPlayback({channel, sequence, payload}),
        };
      default:
        console.log('DRW', channel, payload.toString('hex'));
        return {channel, sequence};
    }
  }

  decodeDrwPlayback({channel, sequence, payload}) {
    const starter = payload[4] === 0x55 && payload[5] === 0xaa;
    const {playbackFilename} = this;

    if (sequence != this.lastPlaybackFrame + 1) {
      if (sequence > this.lastPlaybackFrame) {
        log.playback(`missed ${sequence - this.lastPlaybackFrame} packets`);
        return {sequence: this.lastPlaybackFrame};
      } else if (sequence < this.lastPlaybackFrame) {
        log.playback('repeated packet', sequence);
        return {sequence};
      }
    }

    log.playback(`${this.lastPlaybackFrame} -> ${sequence}`);
    if (starter) {
      const header = payload.slice(0, 12);
      const content = payload.slice(12);
      fs.appendFile(playbackFilename, content, printError);
    } else {
      const header = payload.slice(0, 4);
      const content = payload.slice(4);
      fs.appendFile(playbackFilename, content, printError);
    }
    this.lastPlaybackFrame = sequence;

    return {channel, sequence};
  }

  decodeDrwVideo({channel, sequence, payload}) {
    const starter = payload[4] === 0x55 && payload[5] === 0xaa;
    const header = payload.slice(0, starter ? 12 : 4);
    const content = payload.slice(starter ? 12 : 4);
    if (sequence > this.lastFrame || sequence === 0) {
      if (sequence - this.lastFrame > 1) {
        console.log(`lost ${sequence - this.lastFrame - 1} packets`);
      }
      localVideoSocket.send(content, localVideoPort, '127.0.0.1', err => {
        if (err) {
          console.log(err);
          localVideoSocket.close();
        }
      });
      this.lastFrame = sequence;
    }

    return {channel, sequence};
  }

  multipartHandling(sequence, payload) {
    const {buffer, total, lastSequence} = this.partial;

    if (total === 0) {
      // No multipart in progress
      const mimetype = payload[4];
      const length = payload.readUInt16LE(8);
      const body = payload.slice(12, 12 + length);

      // Complete individual packet
      if (body.length >= length) {
        return {sequence, mimetype, body};
      }

      if (sequence > lastSequence) {
        // First part of new multipart
        log.multipart('start of multipart');
        this.partial.buffer = body;
        this.partial.total = length;
        this.partial.mimetype = mimetype;
        this.partial.lastSequence = sequence;
      }
      return {sequence, mimetype, body: null};
    } else if (total > 0) {
      //multipart in progress
      const body = payload.slice(4);
      const completesMultipart = buffer.length + body.length >= total;

      log.multipart(
        `${buffer.length} + ${body.length} / ${total} | ${lastSequence} -> ${sequence}`,
      );

      if (sequence != lastSequence + 1) {
        if (sequence > lastSequence) {
          log.multipart('missed packets', sequence - lastSequence);
        } else if (sequence < lastSequence) {
          log.multipart('repeat packets', lastSequence - sequence);
        }
        return {
          sequence: lastSequence,
          mimetype: this.partial.mimetype,
          body: null,
        };
      }

      if (completesMultipart) {
        // Complete individual packet or completion of multi-packet
        log.multipart('!!!!!!completion of multipart!!!!!!');
        const fullBody = Buffer.concat([buffer, body]);
        const {mimetype} = this.partial;

        // store final sequence to prevent restarting
        this.partial.lastSequence = sequence;

        // Reset partial
        this.partial.buffer = Buffer.alloc(0);
        this.partial.total = 0;
        this.partial.mimetype = 0;
        return {sequence, mimetype, body: fullBody};
      } else {
        // aggregating packets
        log.multipart('piece of multipart', sequence, buffer.length);
        this.partial.buffer = Buffer.concat([buffer, body]);
        this.partial.lastSequence = sequence;
        return {sequence, mimetype: this.partial.mimetype, body: null};
      }
    } else {
      console.log('how did I end up here?');
    }

    return {
      sequence: lastSequence,
      mimetype: this.partial.mimetype,
      body: fullBody,
    };
  }

  decodeDrwText({channel, sequence, payload}) {
    const {
      sequence: lastSequence,
      length,
      mimetype,
      body,
    } = this.multipartHandling(sequence, payload);

    if (body === null) {
      // incomplete body, ack the received packet
      return {channel, sequence: lastSequence};
    }

    switch (mimetype) {
      case 0x01: //text
        log.text(body.toString());
        return {channel, sequence, mimetype, length, body: body.toString()};
      case 0x06: //json
        const obj = JSON.parse(body.toString());
        log.json(obj);
        return {channel, sequence, mimetype, length, body: obj};
      default:
        console.log('unhandled minetype', mimetype.toString(0x10));
        return {channel, sequence, mimetype, length, body};
    }
  }
}

module.exports = {
  CloudClient,
};

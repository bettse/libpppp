const shuffle = Buffer.from(
  '7c9ce84a13dedcb22f2123e4307b3d8cbc0b270c3cf79ae7087196009785efc11fc4dba1c2ebd901faba3b05b81587832872d18b5ad6da9358feaacc6e1bf0a388ab43c00db545384f502266207f075b14981d9ba72ab9a8cbf1fc4947063eb10e043a945eee541134dd4df9ecc7c9e3781a6f706ba4bda95dd5f8e5bb26af4237d8e1020aae5f1cc573094e6924906d12b319ad748a2940f52dbea559e0f479d24bce8982488425c6912ba2fb8fe9a6b09e3f65f603312eac0f952c5ced39b7336c567eb4a0fd7a815351868d9f77ff6a80dfe2bf10d775645776f355cdd0c818e6364162cf99f2324c67606192cad3ea637d16b68ed46835c3529d46441e17',
  'hex',
);

function hashSeed(seed) {
  const hash = Buffer.alloc(4);

  for (var i = 0; i < seed.length; i++) {
    hash[0] = hash[0] ^ seed.charCodeAt(i);
    hash[1] = hash[1] + seed.charCodeAt(i) / 3;
    hash[2] = hash[2] - seed.charCodeAt(i);
    hash[3] = hash[3] + seed.charCodeAt(i);
  }

  hash.reverse();
  return hash;
}

function lookup(hash, b) {
  const index = hash[b & 0x3] + b;
  return shuffle[index % shuffle.length];
}

function decrypt(seed = null, encrypted) {
  if (seed === null || seed.length === 0) {
    return encrypted;
  }

  const hash = hashSeed(seed);
  const clear = Buffer.alloc(encrypted.length);
  var keybyte = lookup(hash, 0);

  clear[0] = encrypted[0] ^ keybyte;
  for (var i = 1; i < encrypted.length; i++) {
    keybyte = lookup(hash, encrypted[i - 1]);
    clear[i] = encrypted[i] ^ keybyte;
  }

  return clear;
}

function encrypt(seed = null, clear) {
  if (seed === null || seed.length === 0) {
    return clear;
  }
  const hash = hashSeed(seed);
  const encrypted = Buffer.alloc(clear.length);
  var keybyte = lookup(hash, 0);

  encrypted[0] = clear[0] ^ keybyte;
  for (var i = 1; i < clear.length; i++) {
    keybyte = lookup(hash, encrypted[i - 1]);
    encrypted[i] = clear[i] ^ keybyte;
  }

  return encrypted;
}

module.exports = {
  encrypt,
  decrypt,
};

const DECIMAL = 10;

function decodeSockAddr(payload) {
  const family = payload.readUInt16BE(0);
  const port = payload.readUInt16LE(2);
  const address = `${payload[7]}.${payload[6]}.${payload[5]}.${payload[4]}`;
  return {family, port, address};
}

function encodeSockAddr({family, port, address}) {
  const buff = Buffer.alloc(15);
  family = family === 'IPv4' ? 2 : 10;
  const parts = address.split('.').map(x => parseInt(x, DECIMAL));
  buff[0] = family;
  buff.writeUInt16LE(port, 1);
  buff[3] = parts[0];
  buff[4] = parts[1];
  buff[5] = parts[2];
  buff[6] = parts[3];
  // 8 bytes of 0
  return buff;
}

function encodeDid(did) {
  const [prefix, serial, check] = did.split('-');
  const serialNumber = parseInt(serial, DECIMAL);
  const buff = Buffer.alloc(17);
  Buffer.from(prefix).copy(buff);
  buff.writeUInt32BE(serialNumber, 8);
  Buffer.from(check).copy(buff, 12);
  return buff;
}

function decodeDid(buff) {
  const serialNumber = buff.readUInt32BE(8);
  const prefixEnd = buff.indexOf(0);
  const prefix = buff.slice(0, prefixEnd).toString();
  const checkEnd = buff.indexOf(0, 12);
  const check = buff.slice(12, checkEnd).toString();

  const serial = serialNumber.toString().padStart(0, 6);
  return `${prefix}-${serial}-${check}`;
}

module.exports = {
  decodeSockAddr,
  encodeSockAddr,
  decodeDid,
  encodeDid,
};
